// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }
}

internal struct SceneType<T: Any> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

internal struct InitialSceneType<T: Any> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

internal protocol SegueType: RawRepresentable { }

internal extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum DetailedInformation: StoryboardType {
    internal static let storyboardName = "DetailedInformation"

    internal static let initialScene = InitialSceneType<places.DetailedInformationController>(storyboard: DetailedInformation.self)
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum List: StoryboardType {
    internal static let storyboardName = "List"

    internal static let initialScene = InitialSceneType<places.ListController>(storyboard: List.self)

    internal static let listController = SceneType<places.ListController>(storyboard: List.self, identifier: "ListController")
  }
  internal enum Map: StoryboardType {
    internal static let storyboardName = "Map"

    internal static let initialScene = InitialSceneType<places.MapController>(storyboard: Map.self)
  }
  internal enum Navigation: StoryboardType {
    internal static let storyboardName = "Navigation"
  }
  internal enum Search: StoryboardType {
    internal static let storyboardName = "Search"

    internal static let initialScene = InitialSceneType<places.SearchController>(storyboard: Search.self)

    internal static let detailController = SceneType<places.SearchController>(storyboard: Search.self, identifier: "DetailController")
  }
}

internal enum StoryboardSegue {
  internal enum Map: String, SegueType {
    case detailControllerSegue = "DetailControllerSegue"
    case detailedInformationSegue = "DetailedInformationSegue"
    case listControllerSegue = "ListControllerSegue"
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
