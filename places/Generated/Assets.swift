// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias Image = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias Image = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

@available(*, deprecated, renamed: "ImageAsset")
internal typealias AssetType = ImageAsset

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: Image {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  #if swift(>=3.2)
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
  #endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let favouritePlace = ImageAsset(name: "FavouritePlace")
  internal static let image = ImageAsset(name: "Image")
  internal static let playPlaces = ImageAsset(name: "PlayPlaces")
  internal static let addToFafourite = ImageAsset(name: "addToFafourite")
  internal static let allPlaceCenterPurple = ImageAsset(name: "allPlaceCenterPurple")
  internal static let allPlaceCenterTransparent = ImageAsset(name: "allPlaceCenterTransparent")
  internal static let allPlaceWhite = ImageAsset(name: "allPlaceWhite")
  internal static let allPlaces = ImageAsset(name: "allPlaces")
  internal static let backNavigation = ImageAsset(name: "backNavigation")
  internal static let brownAnnotation = ImageAsset(name: "brownAnnotation")
  internal static let cinemaPlaces = ImageAsset(name: "cinemaPlaces")
  internal static let contactPhone = ImageAsset(name: "contactPhone")
  internal static let contactTwitter = ImageAsset(name: "contactTwitter")
  internal static let deleteText = ImageAsset(name: "deleteText")
  internal static let distanceIcon = ImageAsset(name: "distanceIcon")
  internal static let drinkPlaces = ImageAsset(name: "drinkPlaces")
  internal static let drinkTransparent = ImageAsset(name: "drinkTransparent")
  internal static let eatPlaces = ImageAsset(name: "eatPlaces")
  internal static let eatWhite = ImageAsset(name: "eatWhite")
  internal static let enlargeMap = ImageAsset(name: "enlargeMap")
  internal static let favourite = ImageAsset(name: "favourite")
  internal static let findPlaces = ImageAsset(name: "findPlaces")
  internal static let findTabIcon = ImageAsset(name: "findTabIcon")
  internal static let finndHeader = ImageAsset(name: "finndHeader")
  internal static let goToUserPosition = ImageAsset(name: "goToUserPosition")
  internal static let head = ImageAsset(name: "head")
  internal static let lightBlueAnnotation = ImageAsset(name: "lightBlueAnnotation")
  internal static let mapITabItem = ImageAsset(name: "mapITabItem")
  internal static let notImage = ImageAsset(name: "notImage")
  internal static let onMap = ImageAsset(name: "onMap")
  internal static let onMapPlace = ImageAsset(name: "onMapPlace")
  internal static let openDetailInfo = ImageAsset(name: "openDetailInfo")
  internal static let openPlaceIcon = ImageAsset(name: "openPlaceIcon")
  internal static let orangeAnnotation = ImageAsset(name: "orangeAnnotation")
  internal static let ovalMedium = ImageAsset(name: "ovalMedium")
  internal static let ovalMin = ImageAsset(name: "ovalMin")
  internal static let placePosition = ImageAsset(name: "placePosition")
  internal static let placesTabIcon = ImageAsset(name: "placesTabIcon")
  internal static let profileTabIcon = ImageAsset(name: "profileTabIcon")
  internal static let purpleAnnotation = ImageAsset(name: "purpleAnnotation")
  internal static let readPlaces = ImageAsset(name: "readPlaces")
  internal static let rectangle8 = ImageAsset(name: "rectangle8")
  internal static let reduceMap = ImageAsset(name: "reduceMap")
  internal static let reference = ImageAsset(name: "reference")
  internal static let referencePlace = ImageAsset(name: "referencePlace")
  internal static let routeToPlace = ImageAsset(name: "routeToPlace")
  internal static let searchBackground = ImageAsset(name: "searchBackground")
  internal static let starOff = ImageAsset(name: "starOff")
  internal static let starOn = ImageAsset(name: "starOn")
  internal static let telephone = ImageAsset(name: "telephone")
  internal static let turquoiseAnnotation = ImageAsset(name: "turquoiseAnnotation")
  internal static let upView = ImageAsset(name: "upView")
  internal static let userPosition = ImageAsset(name: "userPosition")
  internal static let userPositionMerge = ImageAsset(name: "userPositionMerge")

  // swiftlint:disable trailing_comma
  internal static let allColors: [ColorAsset] = [
  ]
  internal static let allImages: [ImageAsset] = [
    favouritePlace,
    image,
    playPlaces,
    addToFafourite,
    allPlaceCenterPurple,
    allPlaceCenterTransparent,
    allPlaceWhite,
    allPlaces,
    backNavigation,
    brownAnnotation,
    cinemaPlaces,
    contactPhone,
    contactTwitter,
    deleteText,
    distanceIcon,
    drinkPlaces,
    drinkTransparent,
    eatPlaces,
    eatWhite,
    enlargeMap,
    favourite,
    findPlaces,
    findTabIcon,
    finndHeader,
    goToUserPosition,
    head,
    lightBlueAnnotation,
    mapITabItem,
    notImage,
    onMap,
    onMapPlace,
    openDetailInfo,
    openPlaceIcon,
    orangeAnnotation,
    ovalMedium,
    ovalMin,
    placePosition,
    placesTabIcon,
    profileTabIcon,
    purpleAnnotation,
    readPlaces,
    rectangle8,
    reduceMap,
    reference,
    referencePlace,
    routeToPlace,
    searchBackground,
    starOff,
    starOn,
    telephone,
    turquoiseAnnotation,
    upView,
    userPosition,
    userPositionMerge,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  internal static let allValues: [AssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

internal extension Image {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX) || os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal extension AssetColorTypeAlias {
  #if swift(>=3.2)
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: asset.name, bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
  #endif
}

private final class BundleToken {}
