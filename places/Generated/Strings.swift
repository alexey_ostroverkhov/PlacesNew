// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
internal enum L10n {

  internal enum List {
    /// Все
    internal static let all = L10n.tr("Localizable", "list.all")
    /// Избранные
    internal static let favourite = L10n.tr("Localizable", "list.favourite")
    /// Новые
    internal static let new = L10n.tr("Localizable", "list.new")
  }

  internal enum Map {

    internal enum Tabitem {
      /// Поиск
      internal static let find = L10n.tr("Localizable", "map.tabItem.find")
      /// Карта
      internal static let map = L10n.tr("Localizable", "map.tabItem.map")
      /// Места
      internal static let places = L10n.tr("Localizable", "map.tabItem.places")
    }
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
  fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
