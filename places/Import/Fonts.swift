//
//  Fonts.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 07.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    // 2 экран адрес (страна, регион)
    // 2,3 экран, расстояние
    // 3 экран адрес, время работы
    // 4 экран текст, кроме заголовков
    class var regular: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .medium)
    }

    // 2 экран адрес (улица, номер дома)
    // 3 экран, название места
    // 4 экран "Галерея"
    class var header: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: .semibold)
    }

    // 1 экран, подпись вкладок
    class var textStyle: UIFont {
        return UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }

    class var textStyle6: UIFont {
        return UIFont(name: "Roboto-Medium", size: 15.0)!
    }

    class var nameCategoryStyle: UIFont {
        return UIFont(name: "Roboto-Regular", size: 12.0)!
    }

    class var textStyle9: UIFont {
        return UIFont.systemFont(ofSize: 34.0, weight: .regular)
    }

    class var textStyle10: UIFont {
        return UIFont.systemFont(ofSize: 13.0, weight: .regular)
    }

    class var textStyle11: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: .light)
    }

    class var textStyle12: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: .semibold)
    }

    class var textStyle13: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .medium)
    }
}
