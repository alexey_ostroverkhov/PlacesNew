//
//  Colors.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 07.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    // 1 экран, подпись вкладок
    @nonobjc class var warmPurple: UIColor {
        return UIColor(red: 102.0 / 255.0, green: 45.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var orangish: UIColor {
        return UIColor(red: 251.0 / 255.0, green: 130.0 / 255.0, blue: 44.0 / 255.0, alpha: 1.0)
    }

    // 2 экран, расстояние
    @nonobjc class var warmGrey: UIColor {
        return UIColor(white: 136.0 / 255.0, alpha: 1.0)
    }

    // 2,3,4 экран, адресс
    // 3 экран, время работы
    @nonobjc class var greyishBrown: UIColor {
        return UIColor(white: 76.0 / 255.0, alpha: 1.0)
    }

    // 3,4 экран, название места
    @nonobjc class var black: UIColor {
        return UIColor(white: 34.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var pinkishGrey: UIColor {
        return UIColor(white: 191.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var namecategoryColor: UIColor {
        return UIColor(white: 151.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var warmGreyThree: UIColor {
        return UIColor(white: 153.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var white: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }

    @nonobjc class var cerise: UIColor {
        return UIColor(red: 237.0 / 255.0, green: 30.0 / 255.0, blue: 121.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var lightEggplant: UIColor {
        return UIColor(red: 122.0 / 255.0, green: 62.0 / 255.0, blue: 146.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var lightEggplant26: UIColor {
        return UIColor(red: 122.0 / 255.0, green: 62.0 / 255.0, blue: 146.0 / 255.0, alpha: 0.26)
    }

    @nonobjc class var lightEggplant13: UIColor {
        return UIColor(red: 122.0 / 255.0, green: 62.0 / 255.0, blue: 146.0 / 255.0, alpha: 0.13)
    }

    @nonobjc class var darkSkyBlue: UIColor {
        return UIColor(red: 86.0 / 255.0, green: 147.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var turtleGreen: UIColor {
        return UIColor(red: 108.0 / 255.0, green: 196.0 / 255.0, blue: 81.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var bluishGreen: UIColor {
        return UIColor(red: 15.0 / 255.0, green: 170.0 / 255.0, blue: 114.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var lightblue: UIColor {
        return UIColor(red: 158.0 / 255.0, green: 195.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var greyishBrown8: UIColor {
        return UIColor(white: 69.0 / 255.0, alpha: 0.08)
    }

    @nonobjc class var greyishBrown6: UIColor {
        return UIColor(white: 69.0 / 255.0, alpha: 0.06)
    }
}
