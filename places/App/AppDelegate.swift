//
//  AppDelegate.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 17.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
