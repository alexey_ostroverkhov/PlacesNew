//
//  EasyDiUi.swift
//  SA-100
//
//  Created by George Kiriy on 17/04/2018.
//  Copyright © 2018 S Media Link. All rights reserved.
//

import EasyDi
import Foundation

protocol AnyResolvingAssembly {
    func inject(into: AnyObject)
}

protocol ResolvingAssembly: AnyResolvingAssembly {
    associatedtype InstanceType: NSObjectProtocol
    func inject(into instance: InstanceType)
}

extension ResolvingAssembly {
    func inject(into any: AnyObject) {
        guard let concrete = any as? InstanceType else {
            fatalError()
        }
        inject(into: concrete)
    }
}

class Application<Delegate>: UIApplication where Delegate: InjectableByTag {
    override var delegate: UIApplicationDelegate? {
        willSet {
            guard let delegate = newValue as? Delegate else {
                fatalError("\(String(describing: newValue)) does not conform to StoryboardInstantiatable")
            }
            delegate.assembly.inject(into: delegate)
        }
    }
}

protocol Injectable: class {
    var assembly: AnyResolvingAssembly { get }
}

protocol InjectableByTag: Injectable {
    associatedtype InstantiationAssembly: Assembly & ResolvingAssembly
}

extension InjectableByTag {
    var assembly: AnyResolvingAssembly {
        return DIContext.defaultInstance.assembly() as InstantiationAssembly
    }
}

extension NSObject {
    @objc var diTag: Any? {
        get {
            return nil
        }
        set {
            if let injectable = self as? Injectable {
                injectable.assembly.inject(into: self)
            }
        }
    }
}
