//
//  Observable.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 22.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//
import Foundation

public final class DisposeBag {
    private var disposables: [Disposable] = []

    public func append(_ disposable: Disposable) {
        disposables.append(disposable)
    }
}

public final class Disposable {
    private let dispose: () -> Void

    init(_ dispose: @escaping () -> Void) {
        self.dispose = dispose
    }

    deinit {
        dispose()
    }

    public func disposed(by disposeBag: DisposeBag) {
        disposeBag.append(self)
    }
}

public final class ObservableValue<T> {
    public typealias Observer = (T) -> Void

    final class Keeper {
        let observer: Observer
        init(_ observer: @escaping Observer) {
            self.observer = observer
        }
    }

    private var observers: [ObjectIdentifier: Keeper] = [:]

    public func observe(_ observer: @escaping Observer) -> Disposable {
        let keeper = Keeper(observer)

        let key = ObjectIdentifier(keeper)
        observers[key] = keeper

        return Disposable {
            self.observers[key] = nil
        }
    }

    fileprivate func next(_ value: T) {
        observers.values.forEach { $0.observer(value) }
    }
}

public final class PublishSubject<T> {
    public let observable: ObservableValue<T> = .init()

    public func next(_ value: T) {
        observable.next(value)
    }
}
