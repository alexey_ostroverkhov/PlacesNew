//
//  ActiveChildrenView.swift
//  RemoteCameraViews
//
//  Created by Evgeniy Abashkin on 16/04/2018.
//  Copyright © 2018 SMediaLink. All rights reserved.
//

import UIKit

open class ActiveChildrenView: UIView {
    open override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            let convertedPoint = convert(point, to: subview)
            if !subview.isHidden && subview.point(inside: convertedPoint, with: event) {
                return true
            }
        }

        return false
    }
}
