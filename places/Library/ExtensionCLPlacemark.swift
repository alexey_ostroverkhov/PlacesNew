//
//  ExtensionCLPlacemark.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 08.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import CoreLocation
import Foundation

extension CLPlacemark {
    func readableAddressStreet() -> String {
        var parts = [String]()

        if let thoroughfare = thoroughfare {
            parts.append(thoroughfare)
        }

        if let subThoroughfare = subThoroughfare {
            parts.append(subThoroughfare)
        }

        return parts.joined(separator: ", ")
    }

    func readableAddressCountry() -> String {
        var parts = [String]()

        if let country = country {
            parts.append(country)
        }

        if let locality = locality {
            parts.append(locality)
        }

        return parts.joined(separator: ", ")
    }
}
