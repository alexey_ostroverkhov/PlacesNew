//
//  PlacesService.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 23.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import PromiseKit
import UIKit

protocol PlacesService {
    func getPlaces() -> Promise<SearchForVenueResponse>
    func getRecommendationVenue(userLocation: Coordinate) -> Promise<[Venue]>
    func getPlacesByCategory(id: String, coordinate: Coordinate, radius: String) -> Promise<[Venue]>
    func getPlacesInLocation(coordinate: Coordinate) -> Promise<[Venue]>
    func getDetailInfo(venueId id: String) -> Promise<VenueDetail>
    func findPlaces(query: String, coordinate: Coordinate) -> Promise<[Venue]>
    func getDetailInfoFromDatabase(venueId id: String) throws -> Promise<[VenueDetail]>
    func saveVenue<T: ManagedTransformable>(_ plain: T)
    func saveSeenPlace(venueId id: String, isFavourite: Bool) throws -> Promise<SeenPlace>
    func getSeenPlace() throws -> Promise<[SeenPlace]>
}

class PlacesServiceImp: PlacesService {
    private let foursquareGateway: FoursquareGateway
    private let persistenceGateway: PersistenceGateway

    init(
        foursquareGateway: FoursquareGateway,
        persistenceGateway: PersistenceGateway
    ) {
        self.foursquareGateway = foursquareGateway
        self.persistenceGateway = persistenceGateway
    }

    func getPlaces() -> Promise<SearchForVenueResponse> {
        return foursquareGateway.perform(TapRequest())
    }

    func getRecommendationVenue(userLocation: Coordinate) -> Promise<[Venue]> {
        return firstly {
            foursquareGateway.perform(ExploreRequest(userLocation))
        }.then { response -> Promise<[Venue]> in
            let groups = response.response.groups.map { $0.items }
            let venues = groups.map { $0.map { $0.venue } }
            let venue = venues.reduce([], { $0 + $1 })
            return Promise.value(venue)
        }
    }

    func getPlacesByCategory(id: String, coordinate: Coordinate, radius: String) -> Promise<[Venue]> {
        return firstly {
            foursquareGateway.perform(RequestSearch(id: id, coordinate: coordinate, radius: radius))
        }.then { request -> Promise<[Venue]> in
            let venues = request.response.venues
            return Promise.value(venues)
        }
    }

    func getPlacesInLocation(coordinate: Coordinate) -> Promise<[Venue]> {
        return firstly {
            foursquareGateway.perform(RequestSearch(coordinate: coordinate))
        }.then { request -> Promise<[Venue]> in
            let venues = request.response.venues
            return Promise.value(venues)
        }
    }

    func getDetailInfo(venueId id: String) -> Promise<VenueDetail> {
        return firstly {
            foursquareGateway.perform(DetailRequest(placeID: id))
        }.then { venue -> Promise<VenueDetail> in
            let venueDetail = venue.response.venue
            return Promise.value(venueDetail)
        }
    }

    func findPlaces(query: String, coordinate: Coordinate) -> Promise<[Venue]> {
        return firstly {
            foursquareGateway.perform(RequestSearch(query: query, coordinate: coordinate))
        }.then { response -> Promise<[Venue]> in
            let venues = response.response.venues
            return Promise.value(venues)
        }
    }

    func getDetailInfoFromDatabase(venueId id: String) throws -> Promise<[VenueDetail]> {
        let venue = try persistenceGateway.get(VenueDetail.self) { results in
            results.filter(NSPredicate(format: "id == %@", id))
        }
        return venue
    }

    func saveSeenPlace(venueId id: String, isFavourite: Bool) throws -> Promise<SeenPlace> {
        return firstly {
            try getDetailInfoFromDatabase(venueId: id)
        }.then { placeArray in
            self.getAndSave(placeArray, id: id)
        }.then { place -> Promise<SeenPlace> in
            let seenPlace = SeenPlace(id: place.id, isFavourite: isFavourite, place: place)
            self.saveVenue(seenPlace)
            return Promise.value(seenPlace)
        }
    }

    private func getAndSave(_ venueList: [VenueDetail], id: String) -> Promise<VenueDetail> {
        if venueList.isEmpty {
            return firstly {
                self.getDetailInfo(venueId: id)
            }.get { venue in
                self.saveVenue(venue)
            }.then { venue in
                Promise.value(venue)
            }
        } else {
            return Promise.value(venueList.first!)
        }
    }

    func saveVenue<T: ManagedTransformable>(_ plain: T) {
        persistenceGateway.save(plain)
    }

    func getSeenPlace() throws -> Promise<[SeenPlace]> {
        let venues = try persistenceGateway.get(SeenPlace.self, configure: nil)
        return venues
    }
}
