//
//  CategoriesService.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import PromiseKit

protocol CategoriesService {
    func getCategories() -> Promise<GetCategory>
}

class CategoriesServiceImp: CategoriesService {
    private let foursquareGateway: FoursquareGateway

    init(
        foursquareGateway: FoursquareGateway
    ) {
        self.foursquareGateway = foursquareGateway
    }

    func getCategories() -> Promise<GetCategory> {
        return foursquareGateway.perform(RequestCategories())
    }
}
