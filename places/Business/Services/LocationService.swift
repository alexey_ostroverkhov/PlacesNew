//
//  LocationService.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 17.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import CoreLocation
import Foundation

class Coordinate: CLLocation {
}

enum LocationAuthorizationStatus {
    case authorized
    case notDetermined
    case denied
}

protocol LocationService {
    func start()
    func getLocation() -> Coordinate?
    func getLocationAuthorizationStatus() -> LocationAuthorizationStatus
    func getDistance(coordinate: Coordinate) -> Double?
    func resetLocation()
    func autorization()
    var locationObservable: ObservableValue<CLLocation> { get }
    var headingObservable: ObservableValue<CLHeading> { get }
}

class LocationServiceImp: NSObject, LocationService {
    private var locationSubject: PublishSubject<CLLocation> = .init()
    private var headingSubject: PublishSubject<CLHeading> = .init()

    private var locationManager = CLLocationManager()
    private var previouslocation: CLLocation? {
        didSet {
            print(previouslocation)
        }
    }

    private var heading = CLHeading()

    var locationObservable: ObservableValue<CLLocation> {
        return locationSubject.observable
    }

    var headingObservable: ObservableValue<CLHeading> {
        return headingSubject.observable
    }

    override init() {
        super.init()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }

    func start() {
        if getLocationAuthorizationStatus() != .authorized {
            autorization()
        } else {
            startUpdationgLocation()
        }
    }

    private func startUpdationgLocation() {
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
    }

    func getLocation() -> Coordinate? {
        guard let location = previouslocation else {
            return nil
        }

        let lat = location.coordinate.latitude
        let lon = location.coordinate.longitude
        return Coordinate(latitude: lat, longitude: lon)
    }

    func getLocationAuthorizationStatus() -> LocationAuthorizationStatus {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .notDetermined:
            return .notDetermined
        case .restricted, .denied:
            return .denied
        case .authorizedAlways, .authorizedWhenInUse:
            return .authorized
        }
    }

    func resetLocation() {
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }

    func getDistance(coordinate: Coordinate) -> Double? {
        return previouslocation?.distance(from: coordinate)
    }

    func autorization() {
        locationManager.requestWhenInUseAuthorization()
    }
}

extension LocationServiceImp: CLLocationManagerDelegate {
    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }

        previouslocation = location
        locationSubject.next(location)
    }

    func locationManager(_: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways,
             .authorizedWhenInUse:
            startUpdationgLocation()

        case .denied,
             .notDetermined,
             .restricted:
            break
        }
    }

    func locationManager(_: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        heading = newHeading
        headingSubject.next(newHeading)
    }
}
