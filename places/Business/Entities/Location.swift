//
//  VenueLocation.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Location: Codable {
    let address: String?
    let lat: Double
    let lng: Double
    var distance: Int?
    let city: String?
    let state: String?
    let country: String?
    let formattedAddress: [String]
}

extension Location {
    static let stub = Location(
        address: "street",
        lat: 0.0,
        lng: 0.0,
        distance: 100,
        city: "City",
        state: "State",
        country: "Coutry",
        formattedAddress: ["Address"]
    )
}
