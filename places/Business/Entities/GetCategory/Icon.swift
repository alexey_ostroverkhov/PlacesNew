//
//  Icon.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Icon: Codable {
    var prefix: String
    var suffix: String
}

extension Icon {
    static let stub = Icon(prefix: "www", suffix: ".ru")
}
