//
//  Category.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Category: Codable {
    var id: String
    var name: String
    var icon: Icon
}

extension Category {
    func getURL() -> URL? {
        let prefix = icon.prefix
        let suffix = icon.suffix
        let size = "64"
        let url = URL(string: "\(prefix)\(size)\(suffix)")

        return url
    }
}
