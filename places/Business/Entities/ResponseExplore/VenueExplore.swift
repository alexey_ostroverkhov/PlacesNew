//
//  VenueExplore.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct VenueExplore: Codable {
    let id: String
    let name: String
    let location: Location
}
