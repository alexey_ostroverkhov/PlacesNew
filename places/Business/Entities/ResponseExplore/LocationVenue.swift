//
//  LocationVenue.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Location: Codable {
    let address: String?
    let lat: Double
    let lng: Double
    let distance: Int
}
