//
//  TimeFrames.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct TimeFrames: Codable {
    let days: String
    let open: [Open]
}

extension TimeFrames {
    static let stub = TimeFrames(days: "", open: [])
}
