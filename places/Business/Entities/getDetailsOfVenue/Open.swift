//
//  Open.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Open: Codable {
    let renderedTime: String
}

extension Open {
    static let stub = Open(renderedTime: "")
}
