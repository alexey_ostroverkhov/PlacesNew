//
//  VenueDetail.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct VenueDetail: Codable {
    let id: String
    let name: String
    let contact: Contact
    var location: Location
    let rating: Double?
    let photos: Photos
    let description: String?
    let page: Page?
    let hours: Hours?
    let bestPhoto: Photo?
}

extension VenueDetail {
    static let stub = VenueDetail(
        id: "111",
        name: "111",
        contact: .stub,
        location: .stub,
        rating: 9.0,
        photos: .stub,
        description: "",
        page: nil,
        hours: .stub,
        bestPhoto: .stub
    )
}
