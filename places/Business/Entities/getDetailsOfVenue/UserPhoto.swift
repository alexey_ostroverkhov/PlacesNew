//
//  UserPhoto.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct UserPhoto: Codable {
    let photo: Photo
}

extension UserPhoto {
    static let stub = UserPhoto(photo: .stub)
}
