//
//  Photos.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Photos: Codable {
    let groups: [GroupsPhoto]
}

extension Photos {
    static let stub = Photos(groups: [])
}
