//
//  Contact.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Contact: Codable {
    let phone: String?
    let formattedPhone: String?
    let twitter: String?
    let instagram: String?
    let facebook: String?
}

extension Contact {
    static let stub = Contact(phone: "",
                              formattedPhone: "",
                              twitter: "",
                              instagram: "",
                              facebook: "")
}
