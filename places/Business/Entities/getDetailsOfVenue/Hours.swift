//
//  Hours.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Hours: Codable {
    let status: String
    let isOpen: Bool
    let timeframes: [TimeFrames]
}

extension Hours {
    static let stub = Hours(status: "", isOpen: true, timeframes: [])
}
