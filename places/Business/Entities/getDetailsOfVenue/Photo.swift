//
//  Photo.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Photo: Codable {
    let prefix: String
    let suffix: String
    let width: Int?
    let height: Int?
}

extension Photo {
    static let stub = Photo(
        prefix: "www",
        suffix: ".png",
        width: 100,
        height: 100
    )
    func getURL() -> URL? {
        let url = URL(string: "\(prefix)\(300)x\(200)\(suffix)")
        return url
    }
}
