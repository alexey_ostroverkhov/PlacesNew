//
//  Page.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct Page: Codable {
    let user: UserPhoto
}

extension Page {
    static let stub = Page(user: .stub)
}
