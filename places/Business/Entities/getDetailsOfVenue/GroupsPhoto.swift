//
//  GroupsPhoto.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct GroupsPhoto: Codable {
    let items: [Photo]
}

extension GroupsPhoto {
    static let stub = GroupsPhoto(items: [])
}
