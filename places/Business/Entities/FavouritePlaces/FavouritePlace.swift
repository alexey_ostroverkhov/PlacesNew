//
//  FavouritePlaces.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 29/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct FavouritePlace: Codable {
    let id: String
}
