//
//  SeenPlace.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 03/09/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

struct SeenPlace: Codable {
    var id: String
    var isFavourite: Bool
    let name: String
    let contact: Contact
    var location: Location
    let rating: Double?
    let photos: Photos
    let description: String?
    let page: Page?
    let hours: Hours?
    let bestPhoto: Photo?

    init(id: String, isFavourite: Bool, place: VenueDetail) {
        self.id = id
        self.isFavourite = isFavourite
        name = place.name
        contact = place.contact
        location = place.location
        rating = place.rating
        photos = place.photos
        description = place.description
        page = place.page
        hours = place.hours
        bestPhoto = place.bestPhoto
    }

    func toVenueDetail() -> VenueDetail {
        return VenueDetail(
            id: id,
            name: name,
            contact: contact,
            location: location,
            rating: rating,
            photos: photos,
            description: description,
            page: page,
            hours: hours,
            bestPhoto: bestPhoto
        )
    }
}
