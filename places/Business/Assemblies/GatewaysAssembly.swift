//
//  GatewaysAssembly.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import EasyDi
import RealmSwift

// swiftformat:disable redundantSelf

final class GatewaysAssembly: Assembly {
    var foursquareGateway: FoursquareGateway {
        return define(init: FoursquareGatewayImp())
    }

    var persistenceGateway: PersistenceGateway {
        return define(
            scope: .lazySingleton,
            init: PersistenceGatewayImp(realmConfig: self.realmConfig) // realmConfig: realmConfig
        )
    }

    var realmConfig = Realm.Configuration(
        schemaVersion: 0,
        deleteRealmIfMigrationNeeded: true
    )
}
