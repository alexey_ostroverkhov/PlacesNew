//
//  ServicesAssembly.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 25.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import EasyDi

// swiftformat:disable redundantSelf

final class ServicesAssembly: Assembly {
    private lazy var gatewaysAssembly: GatewaysAssembly = context.assembly()

    var placesService: PlacesService {
        return define(
            init: PlacesServiceImp(
                foursquareGateway: self.gatewaysAssembly.foursquareGateway,
                persistenceGateway: self.gatewaysAssembly.persistenceGateway
            )
        )
    }

    var locationService: LocationService {
        return define(
            init: LocationServiceImp()
        )
    }

    var categoriesService: CategoriesService {
        return define(
            init: CategoriesServiceImp(
                foursquareGateway: self.gatewaysAssembly.foursquareGateway
            )
        )
    }
}
