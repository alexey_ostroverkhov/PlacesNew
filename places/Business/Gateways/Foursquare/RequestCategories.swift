//
//  RequestCategories.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Alamofire
import PromiseKit

struct RequestCategories: Request {
    typealias Response = GetCategory

    var path: String { return "venues/categories" }
    var method: HTTPMethod { return .get }

    var params: Parameters = [:]

    var encoding: ParameterEncoding = URLEncoding.default

    init() {
    }
}
