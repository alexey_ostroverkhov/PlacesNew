//
//  TapRequest.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Alamofire
import PromiseKit

struct TapRequest: Request {
    typealias Response = SearchForVenueResponse

    var path: String { return "venues/search" }
    var method: HTTPMethod {
        return HTTPMethod.get
    }
    var params: Parameters = [:]
    var encoding: ParameterEncoding = URLEncoding.default

    init() {
        params["ll"] = "45.040442625344, 38.9767300996508"
        params["radius"] = "10"
        params["limit"] = "1"
    }
}
