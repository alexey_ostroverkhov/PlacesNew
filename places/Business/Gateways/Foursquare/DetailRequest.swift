//
//  DetailRequest.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 02.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Alamofire
import PromiseKit

struct DetailRequest: Request {
    typealias Response = GetDetailOfVenue

    private var venueId: String
    var path: String { return "venues/\(venueId)" }
    var method: HTTPMethod { return .get }
    var params: Parameters = [:]
    var encoding: ParameterEncoding = URLEncoding.default

    init(placeID id: String) {
        venueId = id
    }
}
