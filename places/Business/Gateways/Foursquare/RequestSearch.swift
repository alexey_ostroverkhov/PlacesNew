//
//  SearchByCategory.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Alamofire
import PromiseKit

struct RequestSearch: Request {
    typealias Response = SearchForVenueResponse

    var encoding: ParameterEncoding = URLEncoding.default
    var path: String { return "venues/search" }
    var method: HTTPMethod = .get

    var params: Parameters = [:]

    init(id categoryId: String, coordinate: Coordinate, radius: String) {
        params["ll"] = "\(coordinate.coordinate.latitude),\(coordinate.coordinate.longitude)"
        params["categoryId"] = categoryId
        params["radius"] = radius
        params["limit"] = "10"
    }

    init(coordinate: Coordinate) {
        params["ll"] = "\(coordinate.coordinate.latitude),\(coordinate.coordinate.longitude)"
        params["radius"] = "1000"
        params["limit"] = "10"
    }
    init(query: String, coordinate: Coordinate) {
        params["ll"] = "\(coordinate.coordinate.latitude),\(coordinate.coordinate.longitude)"
        params["query"] = query
        params["intent"] = "global"
        params["limit"] = "10"
    }
}
