//
//  FoursquareGateway.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 23.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Alamofire
import PromiseKit

protocol Request {
    associatedtype Response: Decodable

    var path: String { get }
    var method: HTTPMethod { get }
    var params: Parameters { get }
    var encoding: ParameterEncoding { get }
}

protocol FoursquareGateway {
    func perform<T: Request>(_ request: T) -> Promise<T.Response>
}

class FoursquareGatewayImp: FoursquareGateway {
    private let config = Config()

    private let parsingQueue = DispatchQueue.global()

    func createURLRequest<T: Request>(_ request: T) throws -> URLRequest {
        guard let url = config.baseUrl else {
            fatalError("Invalid URL")
        }
        let urlRequest = URLRequest(url: url.appendingPathComponent(request.path))
        var params = request.params
        params["client_id"] = config.clientId
        params["client_secret"] = config.clientSecret
        params["v"] = config.version

        return try request.encoding.encode(urlRequest, with: params)
    }

    func perform<T: Request>(_ request: T) -> Promise<T.Response> {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        return Promise.value(request)
            .then { request -> Promise<URLRequest> in
                Promise.value(try self.createURLRequest(request))
            }
            .then { request in
                Alamofire.request(request).responseData()
            }
            .map(on: parsingQueue) { data, _ -> T.Response in
                // print("ReceivedData", String(data: data, encoding: .utf8))
                try JSONDecoder().decode(T.Response.self, from: data)
            }
            .ensure {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
    }
}
