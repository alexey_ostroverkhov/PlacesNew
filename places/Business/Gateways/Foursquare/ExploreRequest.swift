//
//  ExploreRequest.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Alamofire
import PromiseKit

struct ExploreRequest: Request {
    typealias Response = ExploreVenue

    var path: String { return "venues/explore" }
    var method: HTTPMethod { return .get }

    var params: Parameters = [:]

    var encoding: ParameterEncoding = URLEncoding.default

    init(_ coordinate: Coordinate) {
        params["ll"] = "\(coordinate.coordinate.latitude),\(coordinate.coordinate.latitude)"
    }
}
