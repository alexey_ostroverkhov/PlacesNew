//
//  CachedContact.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedContact: Object {
    @objc dynamic var phone = ""
    @objc dynamic var formattedPhone = ""
    @objc dynamic var twitter = ""
    @objc dynamic var instagram = ""
    @objc dynamic var facebook = ""
}

extension CachedContact: ManagedEntity {
    typealias PlainType = Contact

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        if let phone = plain.phone {
            self.phone = phone
        }
        if let formattedPhone = plain.formattedPhone {
            self.formattedPhone = formattedPhone
        }
        if let twitter = plain.twitter {
            self.twitter = twitter
        }
        if let instagram = plain.instagram {
            self.instagram = instagram
        }
        if let facebook = plain.facebook {
            self.facebook = facebook
        }
    }

    func original() -> PlainType {
        return Contact(phone: phone,
                       formattedPhone: formattedPhone,
                       twitter: twitter,
                       instagram: instagram,
                       facebook: facebook)
    }
}

extension Contact: ManagedTransformable {
    typealias ManagedType = CachedContact
}
