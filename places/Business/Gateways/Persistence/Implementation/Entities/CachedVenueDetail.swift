//
//  CachedVenueDetail.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedVenueDetail: Object, ManagedEntity {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var contact: CachedContact!
    @objc dynamic var location: CachedLocation!
    @objc dynamic var rating = 0.0
    @objc dynamic var photos: CachedPhotos!
    @objc dynamic var descriptionVenue = ""
    @objc dynamic var page: CachedPage?
    @objc dynamic var hours: CachedHours?
    @objc dynamic var bestPhoto: CachedPhoto?

    override static func primaryKey() -> String? {
        return "id"
    }
}

extension CachedVenueDetail {
    typealias PlainType = VenueDetail

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        id = plain.id
        name = plain.name
        contact = CachedContact(plain: plain.contact)
        location = CachedLocation(plain: plain.location)
        photos = CachedPhotos(plain: plain.photos)
        if let rating = plain.rating {
            self.rating = rating
        }
        if let descriptionVenue = plain.description {
            self.descriptionVenue = descriptionVenue
        }
        if let page = plain.page {
            self.page = CachedPage(plain: page)
        }
        if let hours = plain.hours {
            self.hours = CachedHours(plain: hours)
        }
        if let bestPhoto = plain.bestPhoto {
            self.bestPhoto = CachedPhoto(plain: bestPhoto)
        }
    }

    func original() -> PlainType {
        return VenueDetail(
            id: id,
            name: name,
            contact: contact.original(),
            location: location.original(),
            rating: rating,
            photos: photos.original(),
            description: descriptionVenue,
            page: page?.original(),
            hours: hours?.original(),
            bestPhoto: bestPhoto?.original()
        )
    }
}

extension VenueDetail: ManagedTransformable {
    typealias ManagedType = CachedVenueDetail
}
