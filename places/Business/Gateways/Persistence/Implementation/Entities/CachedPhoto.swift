//
//  CachedPhoto.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedPhoto: Object {
    @objc dynamic var prefix = ""
    @objc dynamic var suffix = ""
    @objc dynamic var width = 0
    @objc dynamic var height = 0
}

extension CachedPhoto: ManagedEntity {
    typealias PlainType = Photo

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        prefix = plain.prefix
        suffix = plain.suffix
        if let width = plain.width {
            self.width = width
        }

        if let height = plain.height {
            self.height = height
        }
    }

    func original() -> PlainType {
        return Photo(
            prefix: prefix,
            suffix: suffix,
            width: width,
            height: height
        )
    }
}

extension Photo: ManagedTransformable {
    typealias ManagedType = CachedPhoto
}
