//
//  CachedPhotos.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedPhotos: Object {
    var groups = List<CachedGroupsPhoto>()
}

extension CachedPhotos: ManagedEntity {
    typealias PlainType = Photos

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        groups.append(objectsIn: plain.groups.map { CachedGroupsPhoto(plain: $0) })
    }

    func original() -> PlainType {
        return Photos(groups: groups.map { $0.original() })
    }
}

extension Photos: ManagedTransformable {
    typealias ManagedType = CachedPhotos
}
