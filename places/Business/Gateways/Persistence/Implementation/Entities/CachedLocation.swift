//
//  CachedLocation.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 11.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedLocation: Object {
    @objc dynamic var address = ""
    @objc dynamic var lat = 0.0
    @objc dynamic var lng = 0.0
    @objc dynamic var distance = 0
    @objc dynamic var city = ""
    @objc dynamic var state = ""
    @objc dynamic var country = ""
    @objc dynamic var formattedAddress: String = ""
}

extension CachedLocation: ManagedEntity {
    typealias PlainType = Location

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        if let address = plain.address {
            self.address = address
        }
        lat = plain.lat
        lng = plain.lng
        if let distance = plain.distance {
            self.distance = distance
        }
        if let city = plain.city {
            self.city = city
        }
        if let state = plain.state {
            self.state = state
        }
        if let country = plain.country {
            self.country = country
        }
        formattedAddress = plain.formattedAddress.joined(separator: ",")
    }

    func original() -> PlainType {
        return Location(address: address,
                        lat: lat,
                        lng: lng,
                        distance: distance,
                        city: city,
                        state: state,
                        country: country,
                        formattedAddress: [formattedAddress]
        )
    }
}

extension Location: ManagedTransformable {
    typealias ManagedType = CachedLocation
}
