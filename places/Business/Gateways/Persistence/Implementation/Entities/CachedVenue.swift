//
//  CachedVenue.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 11.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedVenue: Object, ManagedEntity {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var location: CachedLocation!

    override static func primaryKey() -> String? {
        return "id"
    }
}

extension CachedVenue {
    typealias PlainType = Venue

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        location = CachedLocation()
        id = plain.id
        name = plain.name
    }

    func original() -> PlainType {
        return Venue(
            location: .stub,
            id: id,
            name: name
        )
    }
}

extension Venue: ManagedTransformable {
    typealias ManagedType = CachedVenue
}
