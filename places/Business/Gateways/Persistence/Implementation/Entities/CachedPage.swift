//
//  CachedPage.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedPage: Object {
    @objc dynamic var user: CachedUserPhoto!
}

extension CachedPage: ManagedEntity {
    typealias PlainType = Page
    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        user = CachedUserPhoto(plain: plain.user)
    }

    func original() -> PlainType {
        return Page(user: user.original())
    }
}

extension Page: ManagedTransformable {
    typealias ManagedType = CachedPage
}
