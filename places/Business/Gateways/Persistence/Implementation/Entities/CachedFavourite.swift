//
//  CachedFavourite.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 17.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedFavourite: Object {
    @objc dynamic var favouritePlaceId: String = ""

    override static func primaryKey() -> String? {
        return "favouritePlaceId"
    }
}

extension CachedFavourite: ManagedEntity {
    typealias PlainType = FavouritePlace

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        favouritePlaceId = plain.id
    }

    func original() -> PlainType {
        return FavouritePlace(id: favouritePlaceId)
    }
}

extension FavouritePlace: ManagedTransformable {
    typealias ManagedType = CachedFavourite
}
