//
//  CachedCategory.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 11.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedCategory: Object, ManagedEntity {
    typealias PlainType = Category

    @objc dynamic var id = ""
    @objc dynamic var name = ""

    override static func primaryKey() -> String? {
        return "id"
    }
}

extension CachedCategory {
    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        id = plain.id
        name = plain.name
    }

    func original() -> PlainType {
        return Category(
            id: id,
            name: name,
            icon: Icon.stub
        )
    }
}

extension Category: ManagedTransformable {
    typealias ManagedType = CachedCategory
}
