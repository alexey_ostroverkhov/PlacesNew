//
//  CachedHours.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedHours: Object {
    @objc dynamic var status = ""
    @objc dynamic var isOpen = true
}

extension CachedHours: ManagedEntity {
    typealias PlainType = Hours
    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        status = plain.status
        isOpen = plain.isOpen
    }

    func original() -> PlainType {
        return Hours(status: status, isOpen: isOpen, timeframes: [.stub])
    }
}

extension Hours: ManagedTransformable {
    typealias ManagedType = CachedHours
}
