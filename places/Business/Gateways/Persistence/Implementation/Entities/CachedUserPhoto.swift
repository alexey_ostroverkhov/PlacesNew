//
//  CachedUserPhoto.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedUserPhoto: Object {
    @objc dynamic var photo: CachedPhoto!
}

extension CachedUserPhoto: ManagedEntity {
    typealias PlainType = UserPhoto
    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        photo = CachedPhoto(plain: plain.photo)
    }

    func original() -> PlainType {
        return UserPhoto(photo: photo.original())
    }
}

extension UserPhoto: ManagedTransformable {
    typealias ManagedType = CachedUserPhoto
}
