//
//  CachedGroupsPhoto.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 13.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import RealmSwift

final class CachedGroupsPhoto: Object {
    var items = List<CachedPhoto>()
}

extension CachedGroupsPhoto: ManagedEntity {
    typealias PlainType = GroupsPhoto

    convenience init(plain: PlainType) {
        self.init()
        set(with: plain)
    }

    func set(with plain: PlainType) {
        items.append(objectsIn: plain.items.map { CachedPhoto(plain: $0) })
    }

    func original() -> PlainType {
        return GroupsPhoto(items: items.map { $0.original() })
    }
}

extension GroupsPhoto: ManagedTransformable {
    typealias ManagedType = CachedGroupsPhoto
}
