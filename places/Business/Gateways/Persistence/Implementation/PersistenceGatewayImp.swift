//
//  PersistenceGatewayImp.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 10.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import PromiseKit
import RealmSwift

protocol ManagedEntity {
    associatedtype PlainType: ManagedTransformable
    init(plain: PlainType)
    func set(with plain: PlainType)
    func original() -> PlainType
}

protocol ManagedTransformable {
    associatedtype ManagedType: Object, ManagedEntity where ManagedType.PlainType == Self
}

class PersistenceGatewayImp: PersistenceGateway {
    let realmConfig: Realm.Configuration

    init(realmConfig: Realm.Configuration) {
        self.realmConfig = realmConfig
    }

    func realm() throws -> Realm {
        let realm = try Realm(configuration: realmConfig)
        return realm
    }

    func save<T: ManagedTransformable>(_ plain: T) {
        do {
            let realm = try self.realm()

            try realm.write {
                let managed = T.ManagedType()
                managed.set(with: plain)
                realm.add(managed, update: true)
            }
        } catch {
            print(error)
        }
    }

    func get<T: ManagedTransformable>(
        _: T.Type,
        configure: ((Results<T.ManagedType>) -> (Results<T.ManagedType>))?
    ) throws -> Promise<[T]> {
        return firstly {
            Promise.value(try realm())
        }.then { realm in
            Promise.value(realm.objects(T.ManagedType.self))
        }.map { result in
            var result = result
            if let configure = configure {
                result = configure(result)
            }
            return result.map { $0.original() }
        }
    }

    func sample() throws {
        let venue = Venue(location: .stub, id: "ddd", name: "Nameee")
        save(venue)

        let venueDetail = VenueDetail.stub
        save(venueDetail)

        firstly {
            try get(VenueDetail.self, configure: nil)
        }.done {
            print($0)
        }
        print("PersistenceGatewayImp sample")
    }
}
