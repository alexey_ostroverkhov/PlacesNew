//
//  PersistenceGateway.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 10.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import PromiseKit
import RealmSwift

typealias RequestConfigurator<T: ManagedTransformable> = ((Results<T.ManagedType>) -> (Results<T.ManagedType>))

protocol PersistenceGateway {
    func sample() throws

    func get<T: ManagedTransformable>(_: T.Type, configure: RequestConfigurator<T>?) throws -> Promise<[T]>
    func save<T: ManagedTransformable>(_ plain: T)
}

extension PersistenceGateway {
    func get<T: ManagedTransformable>(_ type: T.Type) throws -> Promise<[T]> {
        return try get(type, configure: nil)
    }
}
