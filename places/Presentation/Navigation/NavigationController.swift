//
//  NavigationController.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 30.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import UIKit

class NavigationController: UITabBarController {
    @IBOutlet var tabBarNavigation: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarNavigation.tintColor = UIColor.warmPurple
        tabBarNavigation.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
    }
}
