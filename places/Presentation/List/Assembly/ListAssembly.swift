//
//  FindAssembly.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 06.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import EasyDi
import Foundation

// swiftformat:disable redundantSelf

final class ListAssembly: Assembly, ResolvingAssembly {
    private lazy var servicesAssembly: ServicesAssembly = context.assembly()

    func inject(into view: ListController) {
        defineInjection(key: "view", into: view) {
            $0.viewModel = self.viewModel
            return $0
        }
    }

    var view: ListViewModelOutput {
        return definePlaceholder()
    }

    var viewModel: ListViewModel {
        return define(
            init:
            ListViewModelImp(
                locationService: self.servicesAssembly.locationService,
                placesService: self.servicesAssembly.placesService,
                output: self.view
            )
        )
    }
}

extension ListController: InjectableByTag {
    typealias InstantiationAssembly = ListAssembly
}
