//
//  ListController.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 17.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Kingfisher
import PromiseKit
import Reusable
import UIKit

class ListController: UIViewController {
    var viewModel: ListViewModel!
    var places: [SeenPlace] = []
    var favourite = false

    @IBOutlet var btnAll: UIButton!
    @IBOutlet var btnFavourite: UIButton!

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        viewModel.getSeenPlaces()
        createTable()
    }

    func createTable() {
        tableView.register(cellType: ListCellControllerTableViewCell.self)
        tableView.layer.cornerRadius = 15
        tableView.dataSource = self
    }

    @IBAction func showAllPlaces(_: UIButton) {
        favourite = false
        places = viewModel.places.filter { $0.isFavourite == false }
        tableView.reloadData()
        btnAll.titleLabel?.font = UIFont.header
        btnFavourite.titleLabel?.font = UIFont.regular
    }

    @IBAction func showFavouritePlaces(_: UIButton) {
        favourite = true
        places = viewModel.places.filter { $0.isFavourite == true }
        tableView.reloadData()
        btnFavourite.titleLabel?.font = UIFont.header
        btnAll.titleLabel?.font = UIFont.regular
    }

    override func viewWillAppear(_: Bool) {
    }
}

extension ListController: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return places.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as ListCellControllerTableViewCell
        cell.delegate = self
        cell.isFavourite = places[indexPath.row].isFavourite
        let venue = places[indexPath.row].toVenueDetail()
        cell.fill(venue)
        return cell
    }
}

extension ListController: ListCellControllerTableViewCellDelegate {
    func save(id: String, favourite: Bool) {
        viewModel.save(id: id, favourire: favourite)
        viewModel.getSeenPlaces()
        //tableView.reloadData()
    }

    func goToMap() {
        //tabBarController?.selectedIndex = 2
    }
}

extension ListController: ListViewModelOutput {
    func reloadData() {
        tableView.reloadData()
    }
}
