//
//  ListCellControllerTableViewCell.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 06.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Reusable
import UIKit

protocol ListCellControllerTableViewCellDelegate {
    func goToMap()
    func save(id: String, favourite: Bool)
}

class ListCellControllerTableViewCell: UITableViewCell, NibReusable {
    var delegate: ListCellControllerTableViewCellDelegate?
    var placeID: String?
    var isFavourite = false

    @IBOutlet var bestPhoto: UIImageView!
    @IBOutlet var namePlace: UILabel!
    @IBOutlet var addressPlace: UILabel!
    @IBOutlet var openTimePlace: UILabel!
    @IBOutlet var viewPlace: UIView!
    @IBOutlet var openImage: UIImageView!
    @IBOutlet var favouriteBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewPlace.layer.cornerRadius = 15
        viewPlace.layer.masksToBounds = true
        if isFavourite {
            favouriteBtn.backgroundColor = UIColor.warmPurple
        } else {
            favouriteBtn.backgroundColor = UIColor.white
        }
    }

    @IBAction func showOnMap(_: UIButton) {
        delegate?.goToMap()
    }

    @IBAction func addInFavourite(_: UIButton) {
        guard let id = self.placeID else {
            return
        }
        isFavourite = !isFavourite
        delegate?.save(id: id, favourite: isFavourite)
    }

    func fill(_ place: VenueDetail) {
        placeID = place.id
        addressPlace.text = place.location.address
        namePlace.text = place.name

        openTimePlace.text = place.hours?.status ?? ""

        if let photo = place.bestPhoto {
            let prefix = photo.prefix
            let suffix = photo.suffix

            guard let width = photo.width else {
                return
            }
            guard let height = photo.height else {
                return
            }

            let url = URL(string: "\(prefix)\(width)x\(height)\(suffix)")
            bestPhoto.kf.setImage(with: url)
        } else {
            bestPhoto.image = #imageLiteral(resourceName: "notImage")
        }
        if isFavourite {
            favouriteBtn.backgroundColor = UIColor.warmPurple
            favouriteBtn.tintColor = UIColor.white
        } else {
            favouriteBtn.backgroundColor = UIColor.white
            favouriteBtn.tintColor = UIColor.warmPurple
        }
    }
}
