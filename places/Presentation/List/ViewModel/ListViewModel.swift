//
//  ListViewModel.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 06.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import PromiseKit

protocol ListViewModel {
    var places: [SeenPlace] { get }
    func viewDidLoad()
    func getCurrentLocation() -> Coordinate?
    func getSeenPlaces()
    func save(id: String, favourire: Bool)
}

protocol ListViewModelOutput {
    func reloadData()
}

class ListViewModelImp: ListViewModel {
    private var placesService: PlacesService
    private var locationService: LocationService
    var places: [SeenPlace] = []
    private var output: ListViewModelOutput!

    init(
        locationService: LocationService,
        placesService: PlacesService,
        output: ListViewModelOutput
    ) {
        self.locationService = locationService
        self.placesService = placesService
        self.output = output
    }

    func viewDidLoad() {
        print("ViewDidLoad")
    }

    private func getAndSave(_ venueList: [VenueDetail], id: String) -> Promise<VenueDetail> {
        if venueList.isEmpty {
            return firstly {
                self.placesService.getDetailInfo(venueId: id)
            }.get { venue in
                self.placesService.saveVenue(venue)
            }.then { venue in
                Promise.value(venue)
            }
        } else {
            return Promise.value(venueList.first!)
        }
    }

    func getCurrentLocation() -> Coordinate? {
        return locationService.getLocation()
    }

    func getSeenPlaces() {
        firstly {
            try placesService.getSeenPlace()
        }.done {
            self.places = $0
        }
    }

    func save(id: String, favourire: Bool) {
        do {
            try placesService.saveSeenPlace(venueId: id, isFavourite: favourire)
        } catch {
            print(error)
        }
    }
}
