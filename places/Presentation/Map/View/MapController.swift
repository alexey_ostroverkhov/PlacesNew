//
//  MapController.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 17.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import CoreLocation
import Mapbox
import UIKit

enum ControllerIndex: Int {
    case search
    case places
    case map

    func getTitle() -> String {
        switch self {
        case .map:
            return L10n.Map.Tabitem.map
        case .places:
            return L10n.Map.Tabitem.places
        case .search:
            return L10n.Map.Tabitem.find
        }
    }
}

class MapController: UIViewController {
    var viewModel: MapViewModel!
    // private var
    private var injectionTest: Int = 0
    private var oldPoint = TapLocationAnnotation()
    private var newPoint = TapLocationAnnotation()
    private var userAnnotation: UserLocationAnnotation?
    private var currentLocation: CLLocation?
    private var currentHeading: CLHeading?
    private var viewUserLocation = CustomUserLocationAnnotationView(reuseIdentifier: nil)
    private var isFollow = true
    private let initZoomLevel = 16.0
    private let lengthFormatter = LengthFormatter()
    private let numberFormatter = NumberFormatter()
    private var mapView: MGLMapView!
    private var placesAnnotation: [PlacesLocationAnnotation] = []
    private var placesOutput: MapControllerDelegateOutput?
    // IBOutlet
    @IBOutlet var mapViewTest: UIView!
    @IBOutlet var listContainer: UIView!
    @IBOutlet var detailContainer: UIView!
    @IBOutlet var detailedInformation: ActiveChildrenView!
    @IBOutlet var tabBar: UITabBar!
    @IBOutlet var plusBtn: RoundButton!
    @IBOutlet var minusBtn: RoundButton!
    @IBOutlet var followUser: RoundButton!
    //tapOnMapInfo
    @IBOutlet private var tapOnMapInfo: UIView!
//    @IBOutlet private var locationIcon: UIImageView!
    @IBOutlet private var addressStreet: UILabel!
    @IBOutlet private var addressCountry: UILabel!
//    @IBOutlet private var distanceIcon: UIImageView!
    @IBOutlet private var distanceText: UILabel!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var manageMapStackView: UIStackView!
    // placeView
    @IBOutlet var placeView: UIView!
    @IBOutlet var placeImage: UIImageView!
    @IBOutlet var placeName: UILabel!
    @IBOutlet var placeAddress: UILabel!
    @IBOutlet var placeOpenTime: UILabel!
    @IBOutlet var placeDistance: UILabel!
    private var place: VenueDetail?
    var delegate: MapControllerDelegateOutput?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        setFieldView()
        mapIsReady()
        setFormatter()
        startSelectedTabItem()
        detailedInformation.isHidden = true
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        guard let segueIdentifier = segue.identifier else {
            return
        }
        switch segueIdentifier {
        case "DetailControllerSegue":
            guard let searchController = segue.destination as? SearchController else {
                break
            }
            searchController.viewModel.setPlacesOutput(viewModel)

        case "DetailedInformationSegue":
            guard let detailedInformationController = segue.destination as? DetailedInformationController else {
                break
            }
            delegate = detailedInformationController

        //  detailedInformationController.viewModel.setPlacesOutput
        default:
            break
        }
    }

//    let detailedViewModel:

    // IBAction
    @IBAction func zoomIn(_: UIButton) {
        mapView.setZoomLevel(mapView.zoomLevel + 1, animated: true)
    }

    @IBAction func zoomOut(_: Any) {
        mapView.setZoomLevel(mapView.zoomLevel - 1, animated: true)
    }

    @IBAction func goToUserPosition(_: Any) {
        guard let coord = currentLocation else {
            return
        }
        mapView.setCenter(coord.coordinate, animated: true)
    }

    func setFormatter() {
        lengthFormatter.unitStyle = .short
        numberFormatter.numberStyle = .none
        lengthFormatter.numberFormatter = numberFormatter
    }

    @objc @IBAction func handleMapTap(sender: UITapGestureRecognizer) {
        let tapPoint: CGPoint = sender.location(in: mapView)
        let tapCoordinate: CLLocationCoordinate2D = mapView.convert(tapPoint, toCoordinateFrom: nil)

        newPoint.coordinate.latitude = tapCoordinate.latitude
        newPoint.coordinate.longitude = tapCoordinate.longitude
        viewModel.getAddress(coordinate: CLLocation(latitude: newPoint.coordinate.latitude, longitude: newPoint.coordinate.longitude))

        tapOnMapInfo.isHidden = false
        detailedInformation.isHidden = true

        mapView.removeAnnotation(oldPoint)
        mapView.addAnnotation(newPoint)
        oldPoint = newPoint
    }

    // private func
    private func mapIsReady() {
        createMap()
        singleMapTap()
    }

    private func createMap() {
        mapView = MGLMapView(frame: view.bounds)

        mapView.setCenter(CLLocationCoordinate2D(latitude: 40.7326808, longitude: -73.9843407), zoomLevel: initZoomLevel, animated: false)
        mapView.delegate = self

        mapViewTest.insertSubview(mapView, at: 0)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: mapViewTest.topAnchor),
            mapView.leftAnchor.constraint(equalTo: mapViewTest.leftAnchor),
            mapView.rightAnchor.constraint(equalTo: mapViewTest.rightAnchor),
            mapView.bottomAnchor.constraint(equalTo: mapViewTest.bottomAnchor),
        ])
    }

    private func singleMapTap() {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(sender:)))

        guard let gestureRecognizers = mapView.gestureRecognizers else {
            return
        }
        for recognizer in gestureRecognizers where recognizer is UITapGestureRecognizer {
            singleTap.require(toFail: recognizer)
        }
        mapView.addGestureRecognizer(singleTap)
    }

    func setFieldView() {
        tapOnMapInfo.isHidden = true
        tapOnMapInfo.clipsToBounds = true
        tapOnMapInfo.layer.cornerRadius = 15
        tapOnMapInfo.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        addressStreet.font = UIFont.header
        addressStreet.textColor = UIColor.greyishBrown

        addressCountry.font = UIFont.regular
        addressCountry.textColor = UIColor.greyishBrown

        distanceText.font = UIFont.regular
        distanceText.textColor = UIColor.warmGrey
    }

    private func applyLocation() {
        if let userAnnotation = userAnnotation, let currentLocation = currentLocation {
            userAnnotation.coordinate = currentLocation.coordinate
        }
    }

    private func updateHeading(annotation: CustomUserLocationAnnotationView) -> CustomUserLocationAnnotationView? {
        if currentLocation == nil {
            return nil
        }
        if let heading = currentHeading?.trueHeading {
            annotation.arrow.isHidden = false

            // Get the difference between the map’s current direction and the user’s heading, then convert it from degrees to radians.
            guard let mapView = mapView else {
                return nil
            }
            let rotation: CGFloat = -MGLRadiansFromDegrees(mapView.direction - heading)

            // If the difference would be perceptible, rotate the arrow.
            if fabs(rotation) > 0.01 {
                // Disable implicit animations of this rotation, which reduces lag between changes.
                CATransaction.begin()
                CATransaction.setDisableActions(true)
                annotation.arrow.setAffineTransform(CGAffineTransform.identity.rotated(by: rotation))
                CATransaction.commit()
            }
        } else {
            annotation.arrow.isHidden = true
        }
        return annotation
    }

    private func startSelectedTabItem() {
        let index = ControllerIndex.map
        let selectedItem = tabBar.items?[index.rawValue]
        tabBar.selectedItem = selectedItem
        tabBar.tintColor = UIColor.lightEggplant

        guard let items = tabBar.items else {
            fatalError()
        }
        items[index.rawValue].title = index.getTitle()
        detailContainer.isHidden = true
        listContainer.isHidden = true
    }
}

// extension
extension MapController: MGLMapViewDelegate {
    func mapView(_: MGLMapView, didFinishLoading _: MGLStyle) {
        userAnnotation = UserLocationAnnotation()
        mapView.addAnnotation(userAnnotation!)
        applyLocation()
    }

    func mapView(_: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        switch annotation {
        case is UserLocationAnnotation:
            return viewUserLocation
        case is PlacesLocationAnnotation:
            let placesLocationAnnotation = annotation as? PlacesLocationAnnotation
            let annotation = CustomPlacesAnnotationView(reuseIdentifier: placesLocationAnnotation!.id)
            // print(placesLocationAnnotation!.id)
            annotation.delegate = self
            annotation.setColor(.blue)
            return annotation
        case is TapLocationAnnotation:
            return nil
        default:
            return nil
        }
    }

    func mapView(_: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        switch annotation {
        case is UserLocationAnnotation:
            return nil
        case is PlacesLocationAnnotation:
            return nil
        case is TapLocationAnnotation:
            let identifer = "placePosition"
            let annotation = CustomTapAnnotationImage(image: #imageLiteral(resourceName: "placePosition"), reuseIdentifier: identifer)
            return annotation
        default:
            return nil
        }
    }
}

extension MapController: MapViewModelOutput {
    func locationChanged(coordinate: CLLocation) {
        currentLocation = coordinate
        applyLocation()
    }

    func headingChanged(heading: CLHeading) {
        currentHeading = heading
        if let viewUserLocation = updateHeading(annotation: viewUserLocation) {
            self.viewUserLocation = viewUserLocation
        }
    }

    func reloadData() {
        tapOnMapInfo.isHidden = false
        if let placeAddress = viewModel.place.first {
            addressStreet.text = placeAddress.readableAddressStreet()
            addressCountry.text = placeAddress.readableAddressCountry()
        }

        guard let userLocation = currentLocation else {
            return
        }
        let tapLocation = newPoint.coordinate
        let clTapLocation = CLLocation(latitude: tapLocation.latitude, longitude: tapLocation.longitude)
        let clUserlocation = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let dist = clUserlocation.distance(from: clTapLocation)
        distanceText.text = lengthFormatter.string(fromMeters: dist)
    }

    func reloadPlaces(places: [VenueDetail]) {
        mapView.removeAnnotations(placesAnnotation)
        placesAnnotation = places.map {
            let lat = $0.location.lat
            let lng = $0.location.lng
            let point = PlacesLocationAnnotation()
            point.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            point.id = $0.id
            return point
        }
        mapView.addAnnotations(placesAnnotation)
    }
}

extension MapController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let tag = ControllerIndex(rawValue: item.tag) else {
            fatalError()
        }

        guard let items = tabBar.items else {
            fatalError("tabBar not found")
        }

        let searchIndex = ControllerIndex.search
        let placesIndex = ControllerIndex.places
        let mapIndex = ControllerIndex.map

        switch tag {
        case .search:
            detailContainer.isHidden = false
            listContainer.isHidden = true
            detailedInformation.isHidden = true
            tapOnMapInfo.isHidden = true

            items[searchIndex.rawValue].title = searchIndex.getTitle()
            items[placesIndex.rawValue].title = ""
            items[mapIndex.rawValue].title = ""

        case .places:
            detailContainer.isHidden = true
            listContainer.isHidden = false
            tapOnMapInfo.isHidden = true
            detailedInformation.isHidden = true

            items[searchIndex.rawValue].title = ""
            items[placesIndex.rawValue].title = placesIndex.getTitle()
            items[mapIndex.rawValue].title = ""

        case .map:
            detailContainer.isHidden = true
            listContainer.isHidden = true
            tapOnMapInfo.isHidden = true
            detailedInformation.isHidden = true

            items[searchIndex.rawValue].title = ""
            items[placesIndex.rawValue].title = ""
            items[mapIndex.rawValue].title = mapIndex.getTitle()
        }
    }
}

protocol MapControllerDelegateOutput {
    func updatePlace(place: VenueDetail)
}

extension MapController: AnnotationDelegate {
    func save(place id: String) {
        viewModel.save(id: id)
    }

    func showView(place id: String) {
        detailedInformation.isHidden = false
        detailContainer.isHidden = true
        tapOnMapInfo.isHidden = true
        mapView.removeAnnotation(newPoint)
        let selectedItem = tabBar.items?[ControllerIndex.map.rawValue]
        tabBar.selectedItem = selectedItem
        place = viewModel.placesDic[id]
        delegate?.updatePlace(place: place!)
    }
}
