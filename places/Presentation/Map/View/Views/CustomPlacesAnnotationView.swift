//
//  CustomPlacesAnnotationView.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import Mapbox
import UIKit

protocol AnnotationDelegate {
    func showView(place id: String)
    func save(place id: String)
}

class CustomPlacesAnnotationView: MGLAnnotationView {
    let size: CGFloat = 20
    var dot: CALayer!
    var tapLayer: CALayer!
    var delegate: AnnotationDelegate?
    var id = ""

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        layer.bounds = CGRect(x: 0, y: 0, width: 2 * size, height: 2 * size)
        setupLayers()
        if let reuseIdentifier = reuseIdentifier {
            id = reuseIdentifier
        }
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated _: Bool) {
        if selected {
            delegate?.showView(place: id)
            delegate?.save(place: id)
            print(id)
        }
    }

    private func setupLayers() {
        if dot == nil {
            tapLayer = CALayer()
            tapLayer.bounds = CGRect(x: 0, y: 0, width: 2 * size, height: 2 * size)
            dot = CALayer()
            dot.bounds = CGRect(x: 0, y: 0, width: size, height: size)
            dot.cornerRadius = size / 2
            dot.borderWidth = 2
            dot.borderColor = UIColor.white.cgColor
            layer.addSublayer(tapLayer)
            layer.addSublayer(dot)
        }
    }

    func setColor(_ color: UIColor) {
        guard let dot = self.dot else {
            return
        }
        dot.backgroundColor = color.cgColor
    }
}
