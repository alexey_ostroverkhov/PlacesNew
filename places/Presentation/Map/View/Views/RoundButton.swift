//
//  RoundButton.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 29/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import UIKit

class RoundButton: UIButton {
    private func shadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowRadius = 100
        layer.shadowOpacity = 1
        layer.masksToBounds = false
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        shadow()
    }

    required override init(frame: CGRect) {
        super.init(frame: frame)
        shadow()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2.0
    }
}
