//
//  CustomTapAnnotationView.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 27/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import Mapbox
import UIKit

class CustomTapAnnotationImage: MGLAnnotationImage {
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init() {
        super.init()
    }
}
