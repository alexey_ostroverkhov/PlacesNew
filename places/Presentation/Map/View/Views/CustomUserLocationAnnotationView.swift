//
//  CustomUserLocationAnnotationView.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 31.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import Mapbox
import UIKit

class CustomUserLocationAnnotationView: MGLAnnotationView {
    let size: CGFloat = 48
    var dot: CALayer!
    var dotMedium: CALayer!
    var dotHight: CALayer!
    var arrow: CAShapeLayer!

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupLayers()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupLayers() {
        if dot == nil {
            dot = CALayer()
            dotMedium = CALayer()
            dotHight = CALayer()

            dot.bounds = CGRect(x: 0, y: 0, width: size, height: size)
            dotMedium.bounds = CGRect(x: 0, y: 0, width: 2 * size, height: 2 * size)
            dotHight.bounds = CGRect(x: 0, y: 0, width: 3 * size, height: 3 * size)

            dot.cornerRadius = size / 2
            dotMedium.cornerRadius = size
            dotHight.cornerRadius = 3 * size / 2

            dot.backgroundColor = UIColor.lightEggplant.cgColor
            dotMedium.backgroundColor = UIColor.lightEggplant26.cgColor
            dotHight.backgroundColor = UIColor.lightEggplant13.cgColor

            layer.addSublayer(dot)
            layer.addSublayer(dotMedium)
            layer.addSublayer(dotHight)
        }

        // This arrow overlays the dot and is rotated with the user’s heading.
        if arrow == nil {
            arrow = CAShapeLayer()
            arrow.path = arrowPath()
            arrow.frame = CGRect(x: 0, y: 0, width: size / 2, height: size / 2)
            arrow.position = CGPoint(x: dot.frame.midX, y: dot.frame.midY)
            arrow.fillColor = UIColor.white.cgColor
            layer.addSublayer(arrow)
        }
    }

    private func arrowPath() -> CGPath {
        let max: CGFloat = size / 2
        let pad: CGFloat = 3

        let top = CGPoint(x: max * 0.5, y: 0)
        let left = CGPoint(x: 0 + pad, y: max - pad)
        let right = CGPoint(x: max - pad, y: max - pad)
        let center = CGPoint(x: max * 0.5, y: max * 0.6)

        let bezierPath = UIBezierPath()
        bezierPath.move(to: top)
        bezierPath.addLine(to: left)
        bezierPath.addLine(to: center)
        bezierPath.addLine(to: right)
        bezierPath.addLine(to: top)
        bezierPath.close()

        return bezierPath.cgPath
    }
}
