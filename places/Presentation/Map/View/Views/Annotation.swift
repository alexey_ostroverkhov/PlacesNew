//
//  Annotation.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 23.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import Mapbox

class UserLocationAnnotation: MGLPointAnnotation {
}

class PlacesLocationAnnotation: MGLPointAnnotation {
    var id: String = ""
}

class TapLocationAnnotation: MGLPointAnnotation {
}
