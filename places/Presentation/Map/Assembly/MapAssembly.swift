//
//  MapAssembly.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 23.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import EasyDi

// swiftformat:disable redundantSelf

final class MapAssembly: Assembly, ResolvingAssembly {
    private lazy var servicesAssembly: ServicesAssembly = context.assembly()

    func inject(into view: MapController) {
        defineInjection(key: "view", into: view) {
            $0.viewModel = self.viewModel
            // $0.injectionTest = 15
            return $0
        }
    }

    var view: MapViewModelOutput {
        return definePlaceholder()
    }

    var viewModel: MapViewModel {
        return define(
            init: MapViewModelImp(
                locationService: self.servicesAssembly.locationService,
                placesService: self.servicesAssembly.placesService,
                output: self.view
            )
        )
    }
}

extension MapController: InjectableByTag {
    typealias InstantiationAssembly = MapAssembly
}
