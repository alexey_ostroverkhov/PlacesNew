//
//  MapViewModel.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 25.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import CoreLocation
import Foundation
import PromiseKit

protocol MapViewModel: SearchViewModelPlacesOutput {
    var place: [CLPlacemark] { get }
    var places: [VenueDetail] { get }
    var placesDic: [String: VenueDetail] { get }
    func getAddress(coordinate: CLLocation)
    func save(id: String)
    func viewDidLoad()
}

protocol MapViewModelOutput {
    func reloadData()
    func locationChanged(coordinate: CLLocation)
    func headingChanged(heading: CLHeading)
    func reloadPlaces(places: [VenueDetail])
}

class MapViewModelImp: MapViewModel {
    private var locationService: LocationService
    private var placesService: PlacesService
    private var output: MapViewModelOutput!
    private var location: CLLocation!
    private let disposeBag: DisposeBag = DisposeBag()

    var places: [VenueDetail] = []
    var place: [CLPlacemark] = []
    var placesDic: [String: VenueDetail] = [:]

    // TODO: use observables
    var locationObservable: ObservableValue<CLLocation> {
        return locationService.locationObservable
    }

    init(
        locationService: LocationService,
        placesService: PlacesService,
        output: MapViewModelOutput
    ) {
        self.locationService = locationService
        self.placesService = placesService
        self.output = output
    }

    func viewDidLoad() {
        locationService.start()

        locationService
            .locationObservable.observe { value in
                self.output.locationChanged(coordinate: value)
            }
            .disposed(by: disposeBag)

        locationService
            .headingObservable.observe { value in
                self.output.headingChanged(heading: value)
            }
            .disposed(by: disposeBag)
    }

    func getAddress(coordinate: CLLocation) {
        let geocoder = CLGeocoder()

        firstly {
            geocoder.reverseGeocode(location: coordinate)
        }.done { placemark in
            self.place = placemark
            self.output.reloadData()
        }.catch { error in
            print(error)
        }
    }

    func save(id: String) {
        do {
            try placesService.saveSeenPlace(venueId: id, isFavourite: false)
        } catch {
            print(error)
        }
    }
}

extension MapViewModelImp: SearchViewModelPlacesOutput {
    func updatePlaces(places: [VenueDetail]) {
        self.places = places
        for temp in places {
            placesDic[temp.id] = temp
        }
        output.reloadPlaces(places: places)
    }
}
