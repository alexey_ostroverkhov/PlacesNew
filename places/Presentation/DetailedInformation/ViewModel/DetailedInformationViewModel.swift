//
//  DetailedInformationViewModel.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 29/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

protocol DetailedInformationViewModel {
}

class DetailedInformationViewModelImp: DetailedInformationViewModel {
    init() {}

    func viewDidLoad() {
    }
}
