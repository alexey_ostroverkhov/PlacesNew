//
//  DetailedInformationAssembly.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 29/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

import EasyDi

// swiftformat:disable redundantSelf

final class DetailedInformationAssembly: Assembly, ResolvingAssembly {
    private lazy var servicesAssembly: ServicesAssembly = context.assembly()

    func inject(into view: DetailedInformationController) {
        defineInjection(key: "view", into: view) {
            $0.viewModel = self.viewModel
            return $0
        }
    }

    var viewModel: DetailedInformationViewModel {
        return define(
            init: DetailedInformationViewModelImp()
        )
    }
}

extension DetailedInformationController: InjectableByTag {
    typealias InstantiationAssembly = DetailedInformationAssembly
}
