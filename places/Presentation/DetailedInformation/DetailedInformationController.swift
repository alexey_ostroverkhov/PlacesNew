//
//  DetailedInformationController.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 28/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Kingfisher
import UIKit

struct ConstaraintsView {
    let top: CGFloat
    let midle: CGFloat
    let bottom: CGFloat
    let hidden: CGFloat
    init(size: CGRect) {
        top = 200
        bottom = size.height - 220
        midle = (top + bottom) / 2
        hidden = size.height
    }

    init() {
        top = 0
        bottom = 0
        midle = 0
        hidden = 0
    }
}

class DetailedInformationController: UIViewController {
    @IBOutlet var infoSpacing: NSLayoutConstraint!
    @IBOutlet var imageHeadView: UIView!
    @IBOutlet var placeInfomationView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var phoneImage: UIImageView!
    @IBOutlet var bestImageHeight: NSLayoutConstraint!

    @IBOutlet var galeryLabel: UILabel!
    @IBOutlet var placeContact: UILabel!
    @IBOutlet var textDescription: UILabel!
    @IBOutlet var galeryCollection: UICollectionView!
    @IBOutlet var placeBestImage: UIImageView!
    @IBOutlet var placeIcon: UIImageView!
    @IBOutlet var placeName: UILabel!
    @IBOutlet var placeAddress: UILabel!
    @IBOutlet var placeDistance: UILabel!
    @IBOutlet var placeDescription: UILabel!
    @IBOutlet var placeTimeWork: UILabel!
    private var photos: [Photo] = []
    var size: ConstaraintsView = ConstaraintsView()
    var viewModel: DetailedInformationViewModel!
    var venue: VenueDetail?
    var spacing: CGFloat?
    @IBOutlet var placePhone: UILabel!

    @IBOutlet var placeRating: RatingControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        imageHeadView.alpha = 0
        placeIcon.layer.cornerRadius = placeIcon.bounds.height / 2
        placeIcon.layer.masksToBounds = true
        placeInfomationView.roundFourCorners()
        scrollView.bounces = false
        scrollView.isScrollEnabled = false
        galeryCollection.register(cellType: CaleryViewCell.self)

        if let spacing = self.spacing {
            infoSpacing.constant = spacing
        }
        galeryCollection.delegate = self
        galeryCollection.dataSource = self
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        size = ConstaraintsView(size: UIScreen.main.bounds)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        infoSpacing.constant = size.bottom
    }

    @IBAction func addFavouite(_: Any) {
    }

    private func setStyle() {
        placeIcon.layer.cornerRadius = placeIcon.bounds.height / 2
        placeName.font = UIFont.header
        placeName.textColor = UIColor.black

        placeAddress.font = UIFont.regular
        placeDistance.font = UIFont.regular
        placeDescription.font = UIFont.regular
        placeTimeWork.font = UIFont.regular
        galeryLabel.font = UIFont.header

        galeryLabel.font = .header
        galeryLabel.textColor = .black
        placeContact.textColor = .black
        placeContact.font = .textStyle6
        placeAddress.textColor = UIColor.greyishBrown
        placeDescription.textColor = UIColor.greyishBrown
        placeDescription.numberOfLines = 0
        // placeDescription.lineBreakMode = .byWordWrapping
        placeTimeWork.textColor = UIColor.greyishBrown
        placeDistance.textColor = UIColor.warmGrey
        galeryLabel.textColor = UIColor.black
    }

    private var isImageVisible: Bool = false {
        didSet {
            guard isImageVisible != oldValue else {
                return
            }

            if isImageVisible {
                imageHeadView.alpha = 0
                imageHeadView.isHidden = false
                UIView.animate(withDuration: 0.5) {
                    self.imageHeadView.alpha = 1
                }
            } else {
                UIView.animate(withDuration: 0.5, animations: {
                    self.imageHeadView.alpha = 0
                }, completion: { _ in
                    self.imageHeadView.isHidden = true
                })
            }
        }
    }

    @IBAction func handlePan(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: view)
        recognizer.setTranslation(.zero, in: view)

        let current = infoSpacing.constant
        let currentHeight = bestImageHeight.constant
        if current + translation.y >= size.top && current + translation.y <= size.hidden {
            infoSpacing.constant = current + translation.y
            bestImageHeight.constant = currentHeight + translation.y
        }
        isImageVisible = infoSpacing.constant <= size.midle

        switch recognizer.state {
        case .ended:
            animateView()
        default:
            break
        }
    }

    private func animateView() {
        switch infoSpacing.constant {
        case size.top ... size.midle:
            infoSpacing.constant = size.top
            bestImageHeight.constant = 240
            UIView.animate(withDuration: 0.3) {
                self.view.layoutSubviews()
                self.imageHeadView.layoutSubviews()
            }
            scrollView.isScrollEnabled = true

        case size.midle ... size.bottom:
            infoSpacing.constant = size.bottom
            bestImageHeight.constant = size.bottom
            UIView.animate(withDuration: 0.3) {
                self.view.layoutSubviews()
                self.imageHeadView.layoutSubviews()
            }
            scrollView.isScrollEnabled = false
            placeDescription.isHidden = false
            textDescription.isHidden = false
            placeContact.isHidden = false

        case size.bottom...:
            bestImageHeight.constant = size.hidden
            infoSpacing.constant = size.hidden
            UIView.animate(withDuration: 0.3) {
                self.view.layoutSubviews()
            }
            placeDescription.isHidden = true
            textDescription.isHidden = true
            placeContact.isHidden = true
            scrollView.isScrollEnabled = false

        default:
            return
        }
    }

    func setField(venue: VenueDetail) {
        if let page = venue.page {
            placeIcon.kf.setImage(with: page.user.photo.getURL())
        } else {
            placeIcon.image = #imageLiteral(resourceName: "notImage")
        }

        if let photo = venue.bestPhoto {
            placeBestImage.kf.setImage(with: photo.getURL())
            placeBestImage.isHidden = false
        } else {
            placeBestImage.isHidden = true
        }

        let arrays = venue.photos.groups.map { $0.items }
        photos = arrays.reduce([], +)

        placeName.text = venue.name
        if let address = venue.location.address {
            placeAddress.isHidden = false
            placeAddress.text = address
        } else {
            placeAddress.isHidden = true
        }

        if let distance = venue.location.distance {
            //    placeDistance.text = String(distance)
            placeDistance.isHidden = true
        }

        if let description = venue.description, description.drop(while: { $0 == " " }) != "" {
            textDescription.isHidden = false
            placeDescription.isHidden = false
            placeDescription.text = description
        } else {
            textDescription.isHidden = true
            placeDescription.isHidden = true
        }

        if let time = venue.hours, time.status.drop(while: { $0 == " " }) != "" {
            placeTimeWork.isHidden = false
            placeTimeWork.text = time.status
        } else {
            placeTimeWork.isHidden = true
        }

        if let phone = venue.contact.formattedPhone, phone.drop(while: { $0 == " " }) != "" {
            placePhone.isHidden = false
            phoneImage.isHidden = false
            placeContact.isHidden = false
            placePhone.text = phone
        } else {
            placeContact.isHidden = true
            placePhone.isHidden = true
            phoneImage.isHidden = true
        }

        if let rating = venue.rating {
            placeRating.rating = Int(rating / 2)
        }
        galeryCollection.reloadData()
    }
}

extension DetailedInformationController: MapControllerDelegateOutput {
    func updatePlace(place: VenueDetail) {
        setField(venue: place)
        infoSpacing.constant = size.bottom
        bestImageHeight.constant = size.bottom
    }
}

extension DetailedInformationController: UICollectionViewDataSource {
    func numberOfSections(in _: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return photos.count
    }

    func collectionView(_: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = galeryCollection.dequeueReusableCell(for: indexPath) as CaleryViewCell
        cell.photo.kf.setImage(with: photos[indexPath.row].getURL())
        return cell
    }
}

extension DetailedInformationController: UICollectionViewDelegateFlowLayout {
    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, insetForSectionAt _: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, minimumLineSpacingForSectionAt _: Int) -> CGFloat {
        return CGFloat(30)
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, minimumInteritemSpacingForSectionAt _: Int) -> CGFloat {
        return CGFloat(30)
    }
}
