//
//  CaleryViewCell.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 28/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Reusable
import UIKit

class CaleryViewCell: UICollectionViewCell, NibReusable {
    @IBOutlet var photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        roundCornerImage()
    }

    private func roundCornerImage() {
        photo.layer.cornerRadius = 8
        photo.layer.masksToBounds = true
    }
}
