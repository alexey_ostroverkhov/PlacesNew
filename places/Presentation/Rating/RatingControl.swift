//
//  RatingControl.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 07.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import UIKit

class RatingControl: UIStackView {
    private var imageViews: [UIImageView] = [
        UIImageView(image: #imageLiteral(resourceName: "starOff")),
        UIImageView(image: #imageLiteral(resourceName: "starOff")),
        UIImageView(image: #imageLiteral(resourceName: "starOff")),
        UIImageView(image: #imageLiteral(resourceName: "starOff")),
        UIImageView(image: #imageLiteral(resourceName: "starOff")),
    ]

    var rating = 0 {
        didSet {
            if rating > 5 {
                rating = 5
            }
            delete()
            setRating()
            addStack()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setRating()
        addStack()
    }

    required init(coder: NSCoder) {
        super.init(coder: coder)
        setRating()
        addStack()
    }

    private func setRating() {
        for i in 0 ..< rating {
            let img = UIImageView()
            img.image = #imageLiteral(resourceName: "starOn")
            imageViews[i] = img
        }
    }

    private func delete() {
        for i in 0 ..< 5 {
            removeArrangedSubview(imageViews[i])
            imageViews[i].removeFromSuperview()
        }
    }

    private func addStack() {
        for i in 0 ..< 5 {
            addArrangedSubview(imageViews[i])
        }
    }
}
