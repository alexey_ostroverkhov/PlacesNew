//
//  SearchViewModel.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 31.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation
import PromiseKit

protocol SearchViewModel {
    var category: [Category] { get }
    var places: [Venue] { get }
    var place: VenueDetail { get }
    var placesDetail: [VenueDetail] { get }
    func viewDidLoad()

    func getPlacesByCategory(id: String, coordinate: Coordinate, radius: String)
    // func getPlacesByExplore()
    func getDetailInfoOfVenue(venueId id: String) throws -> Promise<VenueDetail>
    func getCategory()
    func getPlaces(query: String, coordinate: Coordinate)
    func getDictance(venue: VenueDetail) -> Double?
    func getCoordinate() -> Coordinate?

    func setPlacesOutput(_ placesOutput: SearchViewModelPlacesOutput?)
}

protocol FindViewModelOutput {
    func reloadTableData()
    func reloadCollectionData()
}

protocol FindViewModelOutputMap {
    func reloadPlaces(_ places: [VenueDetail])
}

protocol SearchViewModelPlacesOutput {
    func updatePlaces(places: [VenueDetail])
}

class SearchViewModelImp: SearchViewModel {
    private var locationService: LocationService
    private var placesService: PlacesService
    private var categoriesService: CategoriesService
    private var output: FindViewModelOutput!
    private let disposeBag: DisposeBag = DisposeBag()

    var category: [Category] = []
    var places: [Venue] = []
    var placesDetail: [VenueDetail] = []
    var place: VenueDetail = .stub

    private var placesOutput: SearchViewModelPlacesOutput?

    func setPlacesOutput(_ placesOutput: SearchViewModelPlacesOutput?) {
        self.placesOutput = placesOutput
    }

    init(
        locationService: LocationService,
        placesService: PlacesService,
        categoriesService: CategoriesService,
        output: FindViewModelOutput
    ) {
        self.locationService = locationService
        self.placesService = placesService
        self.categoriesService = categoriesService
        self.output = output
    }

    func viewDidLoad() {
        print("viewDidLoad")
    }

    func getCategory() {
        firstly {
            categoriesService.getCategories()
        }.done { category in
            self.category = category.response.categories
            self.output.reloadCollectionData()
        }.catch { error in
            print(error)
        }
    }

    func getPlacesByCategory(id: String, coordinate: Coordinate, radius: String) {
        placesDetail.removeAll()

        firstly {
            placesService.getPlacesByCategory(id: id, coordinate: coordinate, radius: radius)
        }.done { places in
            self.places = places
            self.output.reloadTableData()
//            self.mapOutput.reloadPlaces(<#T##places: [VenueDetail]##[VenueDetail]#>)
        }.catch { error in
            print(error)
        }
    }

    func getPlacesByExplore(coordinate: Coordinate) {
        placesDetail.removeAll()
        firstly {
            placesService.getRecommendationVenue(userLocation: coordinate)

        }.done { places in
            self.places = places
            self.output.reloadTableData()

        }.catch { error in
            print(error)
        }
    }

    func getDetailInfoOfVenue(venueId: String) throws -> Promise<VenueDetail> {
        return firstly {
            try placesService.getDetailInfoFromDatabase(venueId: venueId)
        }.then { venue in
            self.getAndSave(venue, id: venueId)
        }.get { venue in
            self.placesDetail.append(venue)
            self.placesOutput?.updatePlaces(places: self.placesDetail)
        }
    }

    private func getAndSave(_ venueList: [VenueDetail], id: String) -> Promise<VenueDetail> {
        if venueList.isEmpty {
            return firstly {
                self.placesService.getDetailInfo(venueId: id)
            }.get { venue in
                self.placesService.saveVenue(venue)
            }.then { venue in
                Promise.value(venue)
            }
        } else {
            return Promise.value(venueList.first!)
        }
    }

    func getPlaces(query: String, coordinate: Coordinate) {
        placesDetail.removeAll()
        firstly {
            placesService.findPlaces(query: query, coordinate: coordinate)
        }.done { places in
            self.places = places
            self.output.reloadTableData()
        }.catch { error in
            print(error)
        }
    }

    func getDictance(venue: VenueDetail) -> Double? {
        let lat = venue.location.lat
        let lon = venue.location.lng
        let coord = Coordinate(latitude: lat, longitude: lon)

        return locationService.getDistance(coordinate: coord)
    }

    func getCoordinate() -> Coordinate? {
        return locationService.getLocation()
    }
}
