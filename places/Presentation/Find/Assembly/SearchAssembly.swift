//
//  Assembly.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 01.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Foundation

import EasyDi

// swiftformat:disable redundantSelf

final class SearchAssembly: Assembly, ResolvingAssembly {
    private lazy var servicesAssembly: ServicesAssembly = context.assembly()

    func inject(into view: SearchController) {
        defineInjection(key: "view", into: view) {
            $0.viewModel = self.viewModel
            return $0
        }
    }

    var view: FindViewModelOutput {
        return definePlaceholder()
    }

    var viewModel: SearchViewModel {
        return define(
            init: SearchViewModelImp(
                locationService: self.servicesAssembly.locationService,
                placesService: self.servicesAssembly.placesService,
                categoriesService: self.servicesAssembly.categoriesService,
                output: self.view
            )
        )
    }
}

extension SearchController: InjectableByTag {
    typealias InstantiationAssembly = SearchAssembly
}
