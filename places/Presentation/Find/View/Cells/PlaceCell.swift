//
//  PlaceCell.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 18.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Reusable
import UIKit

protocol PlaceCellDelegate {
    func setDataCell(place: Venue)
}

class PlaceCell: UITableViewCell, NibReusable {
    @IBOutlet var placeImage: UIImageView!
    @IBOutlet var placeName: UILabel!
    @IBOutlet var placeDescription: UILabel!
    @IBOutlet var placeAddress: UILabel!
    @IBOutlet var placeDistance: UILabel!
    @IBOutlet var placeRating: RatingControl!
    @IBOutlet var view: UIView!
    @IBOutlet var widthName: NSLayoutConstraint!
    var placeID = ""

    func setShadow(view: UIView) {
        view.layer.shadowColor = UIColor.greyishBrown6.cgColor
        view.layer.shadowRadius = 30
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 0, height: 10)
        view.layer.masksToBounds = false
    }

    func setStyle() {
        placeName.font = UIFont.header
        placeName.textColor = UIColor.black

        placeDescription.font = UIFont.regular
        placeDescription.textColor = UIColor.greyishBrown

        placeAddress.font = UIFont.regular
        placeAddress.textColor = UIColor.greyishBrown

        placeDistance.font = UIFont.regular
        placeDistance.textColor = UIColor.warmGrey

        placeImage.layer.cornerRadius = placeImage.bounds.width / 2
        placeImage.layer.masksToBounds = true
        view.layer.cornerRadius = 25
        view.layer.masksToBounds = false

        placeDescription.numberOfLines = 2
        placeDescription.lineBreakMode = .byWordWrapping

        setShadow(view: view)
    }

    func fill(venue: VenueDetail) {
        placeID = venue.id
        let lengthFormatter = LengthFormatter()
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .none
        lengthFormatter.numberFormatter = numberFormatter
        lengthFormatter.unitStyle = .short

        if let page = venue.page {
            placeImage.kf.setImage(
                with: page.user.photo.getURL(),
                placeholder: #imageLiteral(resourceName: "notImage"),
                options: nil,
                progressBlock: nil,
                completionHandler: nil
            )
        } else if let bestPhoto = venue.bestPhoto {
            placeImage.kf.setImage(with: bestPhoto.getURL())
        }

        placeName.text = venue.name

        placeDescription.text = venue.description ?? ""
        placeRating.rating = Int(venue.rating ?? 0.0)
        placeAddress.text = venue.location.address ?? ""
        placeDistance.text = String(venue.location.distance ?? 0)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setStyle()
    }
}
