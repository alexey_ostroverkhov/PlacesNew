//
//  CategoryViewCell.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 08.08.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Kingfisher
import Reusable
import UIKit

class CategoryViewCell: UICollectionViewCell, NibReusable {
    @IBOutlet var btnIcon: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        setBtnStyle()
    }

    func setBtnStyle() {
        btnIcon.layer.cornerRadius = btnIcon.bounds.height / 2
        btnIcon.layer.masksToBounds = true
        btnIcon.layer.borderWidth = 2

        btnIcon.isUserInteractionEnabled = false
    }

    func fill(with category: Category) {
        let url = category.getURL()
        let color = tintColor.cgColor
        btnIcon.layer.borderColor = color
        btnIcon.kf.setImage(with: url, for: .normal)
    }
}
