//
//  SearchController.swift
//  places
//
//  Created by Aleksey Ostroverkhov on 18.07.2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

import Kingfisher
import PromiseKit
import Reusable
import UIKit

class SearchController: UIViewController {
    private let colors: [UIColor] = [
        .warmPurple,
        .orangish,
        .darkSkyBlue,
        .turtleGreen,
        .bluishGreen,
        .lightblue,
    ]

    var viewModel: SearchViewModel!

    @IBOutlet var findSpacing: NSLayoutConstraint!
    @IBOutlet var findView: UIView!

    @IBOutlet var expandedSpacing: NSLayoutConstraint!
    @IBOutlet var expandedView: UIView!

    @IBOutlet var gestureRecognizer: UIPanGestureRecognizer!
    @IBOutlet var listPlaces: UITableView!

    @IBOutlet var listCategory: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var stacView: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        findView.setShadow()
        searchBar.setShadow()
        findView.roundTwoCorners()
        findSpacing.constant = 350
        setSearcBarStyle()
        createTable()
        createCollection()
    }

    private var isExpandedVisible: Bool = false {
        didSet {
            guard isExpandedVisible != oldValue else {
                return
            }

            if isExpandedVisible {
                expandedView.alpha = 0
                expandedView.isHidden = false
                UIView.animate(withDuration: 0.5) {
                    self.expandedView.alpha = 1
                }
            } else {
                UIView.animate(withDuration: 0.5, animations: {
                    self.expandedView.alpha = 0
                }, completion: { _ in
                    self.expandedView.isHidden = true
                })
            }
        }
    }

    @IBAction func handlePan(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: view)
        recognizer.setTranslation(.zero, in: view)

        let current = findSpacing.constant

        if current + translation.y >= 150 && current + translation.y <= 350 {
            findSpacing.constant = current + translation.y
        }
        isExpandedVisible = findSpacing.constant < 250

        switch recognizer.state {
        case .ended:
            animateView()
        default:
            break
        }
    }

    private func animateView() {
        if findSpacing.constant > 250 {
            findSpacing.constant = 350
            UIView.animate(withDuration: 0.3) {
                self.view.layoutSubviews()
            }
        } else {
            findSpacing.constant = 150
            UIView.animate(withDuration: 0.3) {
                self.view.layoutSubviews()
            }
        }
    }

    private func setSearcBarStyle() {
        searchBar.delegate = self
    }

    private func createCollection() {
        listCategory.backgroundColor = .white
        listCategory.register(cellType: CategoryViewCell.self)
        listCategory.dataSource = self
        listCategory.delegate = self
        getCategoryIcon()
    }

    private func createTable() {
        listPlaces.register(cellType: PlaceCell.self)
        listPlaces.dataSource = self
    }

    private func getCategoryIcon() {
        viewModel.getCategory()
    }
}

extension SearchController: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return viewModel.places.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as PlaceCell
        let place = viewModel.places[indexPath.row]

        if place.id == cell.placeID {
            return cell
        }

        firstly {
            try viewModel.getDetailInfoOfVenue(venueId: place.id)
        }.done { venue in
            var venue = venue
            if let distance = self.viewModel.getDictance(venue: venue) {
                venue.location.distance = Int(distance)
                cell.fill(venue: venue)
            }
            cell.fill(venue: venue)
        }.catch { error in
            print(error)
        }
        return cell
    }
}

extension SearchController {
    func fillCell(id: String, cell: PlaceCell) throws -> Promise<PlaceCell> {
        return firstly {
            try viewModel.getDetailInfoOfVenue(venueId: id)
        }.map { venue -> PlaceCell in
            var venue = venue
            if let distance = self.viewModel.getDictance(venue: venue) {
                venue.location.distance = Int(distance)
                cell.fill(venue: venue)
            }
            cell.fill(venue: venue)

            return cell
        }
    }
}

extension SearchController: UICollectionViewDataSource {
    func numberOfSections(in _: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        if viewModel.category.count > colors.count {
            return colors.count
        }
        return viewModel.category.count
    }

    func collectionView(_: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = listCategory.dequeueReusableCell(for: indexPath) as CategoryViewCell
        let category = viewModel.category[indexPath.row]

        let color = colors[indexPath.row]
        cell.tintColor = color

        cell.fill(with: category)

        return cell
    }
}

extension SearchController: UICollectionViewDelegate {
    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.getCategory()

        let id = viewModel.category[indexPath.row].id
        guard let coord = viewModel.getCoordinate() else {
            return
        }
        viewModel.getPlacesByCategory(id: id, coordinate: coord, radius: "1000")
    }
}

extension SearchController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard
            let text = searchBar.text,
            let coord = viewModel.getCoordinate()
        else {
            return
        }

        viewModel.getPlaces(query: text, coordinate: coord)
    }
}

extension SearchController: FindViewModelOutput {
    func reloadTableData() {
        listPlaces.reloadData()
    }

    func reloadCollectionData() {
        listCategory.reloadData()
    }
}

extension UIView {
    func roundTwoCorners() {
        clipsToBounds = true
        layer.cornerRadius = 25
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func roundFourCorners() {
        clipsToBounds = true
        layer.cornerRadius = 25
        layer.masksToBounds = true
    }

    func setShadow() {
        layer.shadowColor = UIColor.greyishBrown6.cgColor
        layer.shadowRadius = 30
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 10, height: 10)
        layer.masksToBounds = false
    }
}
