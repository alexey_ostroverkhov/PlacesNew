//
//  ViewsFramework.h
//  ViewsFramework
//
//  Created by Evgeniy Abashkin on 27/08/2018.
//  Copyright © 2018 Aleksey Ostroverkhov. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ViewsFramework.
FOUNDATION_EXPORT double ViewsFrameworkVersionNumber;

//! Project version string for ViewsFramework.
FOUNDATION_EXPORT const unsigned char ViewsFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ViewsFramework/PublicHeader.h>


