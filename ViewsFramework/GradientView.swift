//
//  GradientView.swift
//  KubanEnergy
//
//  Created by Evgeniy Abashkin on 02/12/2017.
//  Copyright © 2017 SMediaLink. All rights reserved.
//

import UIKit

@IBDesignable
open class GradientView: UIView {

    @IBInspectable
    open var beginColor: UIColor = .white {
        didSet {
            updateGradient()
        }
    }

    @IBInspectable
    open var endColor: UIColor = .black {
        didSet {
            updateGradient()
        }
    }

    @IBInspectable
    open var startPoint: CGPoint = CGPoint(x: 0, y: 0) {
        didSet {
            updateGradient()
        }
    }

    @IBInspectable
    open var endPoint: CGPoint = CGPoint(x: 1, y: 0) {
        didSet {
            updateGradient()
        }
    }

    private var gradientLayer: CAGradientLayer!

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    private func configure() {
        gradientLayer = CAGradientLayer()
        layer.insertSublayer(gradientLayer, at: 0)
    }

    private func updateGradient() {
        gradientLayer.colors = [beginColor.cgColor, endColor.cgColor]
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
    }

    override open func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
}
